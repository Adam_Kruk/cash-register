function checkCashRegister(price, cash, cid) {  
    let change = [];  
    let changecid = cid;  
    // if cash-in-drawer is less than the change due, or if you cannot return the exact change  
    let insuff = {    
      status: "INSUFFICIENT_FUNDS",    
      change: []
      }
        //return closed with cash-in-drawer as the value for the key change if it is equal to the change due.  
        let closed = {    
        status: "CLOSED",
        change: [...cid]    
        }  
        // otherwise return open with the change due in coins and bills, sorted in highest to lowest order, as the value of the change key.  
        let open = {    
          status: "OPEN",     
          change    
          }  
  
  let returncash = cash - price;
  
  let value = 0;
  for (let i = 0; i < cid.length; i++){
    value += cid[i][1];
  }
  
  if (returncash == value){
    return closed;
  }
  
  // Hundred dollars 100  
  if (returncash > 100){
  let result = changecid[8][1].toFixed(2) - returncash.toFixed(2);      
  if (result > 0) {        
    for (let i = 1; returncash >= 100; i++) {          
         returncash -= 100;          
         changecid[8][1] = 100 * i;        
         }        
         change.push(changecid[8]);      
         } else {        
           change.push(changecid[8]);        
           returncash -= changecid[8][1].toFixed(2);      
           }    if (returncash <= 0) {      
             return open;    
             }  
    }  
      
    // Twenty dollars 20   
      
    if (returncash > 20){    
        let result = changecid[7][1].toFixed(2) - returncash.toFixed(2);      
        if (result > 0) {        
        for (let i = 1; returncash >= 20; i++) {          
            returncash -= 20;          
            changecid[7][1] = 20 * i;        
        }        
            change.push(changecid[7]);      
            } else {        
            change.push(changecid[7]);        
            returncash -= changecid[7][1].toFixed(2);      
        }    
            if (returncash <= 0) {      
                return open;    
                }  
    }  
      
    // Ten dollars 10   
      
    if (returncash > 10){    
        let result = changecid[6][1].toFixed(2) - returncash.toFixed(2);      
        if (result > 0) {        
        for (let i = 1; returncash >= 10; i++) {          
            returncash -= 10;          
            changecid[6][1] = 10 * i;        
            }
          change.push(changecid[6]);      
          } else {        
            change.push(changecid[6]);        
            returncash -= changecid[6][1].toFixed(2);      
            }    
          if (returncash.toFixed(2) <= 0) {      
            return open;    
            }  
    }  
          
    // Five dollars 5  
    if (returncash > 5){    
        let result = changecid[5][1].toFixed(2) - returncash.toFixed(2);      
        if (result > 0) {        
          for (let i = 1; returncash >= 5; i++) {          
            returncash -= 5;          
            changecid[5][1] = 5 * i;        
            }        
            change.push(changecid[5]);      
            } else {        
              change.push(changecid[5]);        
              returncash -= changecid[5][1];      
              }    
              if (returncash <= 0) {      
                return open;    
                }  
        }  
    
    // Dollar 1  
    if (returncash > 1){    
        let result = changecid[4][1].toFixed(2) - returncash.toFixed(2);      
        if (result > 0) {        
          for (let i = 1; returncash >= 1; i++) {          
            returncash -= 1;          
            changecid[4][1] = 1 * i;        }        
            change.push(changecid[4]);      
            } else {        
              change.push(changecid[4]);        
              returncash -= changecid[4][1];      
              }    
              if (returncash <= 0) {
        return open;    
        }  
    }  
        
    // Quarter 0.25  
    if (returncash > 0.25){      
    let result = changecid[3][1].toFixed(2) - returncash.toFixed(2);      
        if (result > 0) {        
            for (let i = 1; returncash >= 0.25; i++) {                   
                returncash -= 0.25;          
                changecid[3][1] = 0.25 * i;       
                }        
             change.push(changecid[3]);      
             } else {        
                 change.push(changecid[3]);                  
                 returncash -= changecid[3][1];      }    
             if (returncash <= 0) {      
               return open;   
                }  
        }  
        
        // Dime 0.1  
        
        if (returncash > 0.1) {    
          let result = changecid[2][1].toFixed(2) - returncash.toFixed(2);      
          if (result > 0) {        
            for (let i = 1; returncash >= 0.1; i++) {                      
                returncash -= 0.1;          
                changecid[2][1] = 0.1 * i;        
            }        
            change.push(changecid[2]);      
            } else {        
              change.push(changecid[2]);        
              returncash -= changecid[2][1];      
              }    
              if (returncash <= 0) {      
                return open;    
                }  
        }  
                
        // Nickel 0.05  
        if (returncash > 0.05){    
          let result = changecid[1][1].toFixed(2) - returncash.toFixed(2);      
          if (result > 0) {
          for (let i = 1; returncash >= 0.05; i++) {                      
                returncash -= 0.05;          
                    changecid[1][1] = 0.05 * i;        
                    }        
                    change.push(changecid[1]);      
                    } else {        
                        change.push(changecid[1]);        
                        returncash -= changecid[1][1];      
                        }    
                        if (returncash <= 0) {      
                            return open;    
                            }  
            }  
                              
      // Penny 0.01  
      if (returncash > 0.01){    
        let result = changecid[0][1].toFixed(2) - returncash.toFixed(2);      
        if (result > 0) {        
              for (let i = 1; returncash >= 0; i++) {                     
                        returncash -= 0.01;          
                        changecid[0][1] = 0.01 * i;        
                        }        
                        change.push(changecid[0]);      
                        } else {        
                        change.push(changecid[0]);        
                        returncash -= changecid[0][1];      
                    }    
                        if (returncash <= 0) {      
                            return open;    
                            }  
        }  
          
        // insufficient  
        if (returncash > 0){    
            return insuff;  
            }
      }